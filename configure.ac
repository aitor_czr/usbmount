#    -*- Autoconf -*-
# Process this file with autoconf to produce a configure script

AC_PREREQ([2.69])
AC_INIT([usbmount],[0.1],[https://git.devuan.org/aitor_czr/usbmount.git])
AM_INIT_AUTOMAKE([foreign])
AM_SILENT_RULES([yes])
AC_PREFIX_DEFAULT([/usr])

configure_flags="$*"
AC_DEFINE_UNQUOTED([CONFIG_FLAGS], ["$configure_flags"], [Flags passed to configure])

AC_CONFIG_MACRO_DIR([m4])

AC_CONFIG_SRCDIR(src/main.c)

# The earliest version of Libtool that can correctly process the project
LT_PREREQ(2.4.6)
LT_INIT([dlopen])

# ------------- Checks for programs
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_MKDIR_P
AC_PROG_LN_S

AC_CONFIG_SRCDIR([config.h.in])
AC_CONFIG_HEADERS([config.h])

# ------------------------------------------------------------------------------
AC_ARG_ENABLE([manpages], AS_HELP_STRING([--disable-manpages],[disable manpages]),[],[enable_manpages=no])
AM_CONDITIONAL(ENABLE_MANPAGES, [test "x$enable_manpages" = xyes])


# ------------- Checks for header files
AC_HEADER_STDC

# ------------- Library linking checks
AC_LIB_HAVE_LINKFLAGS(ubox, [], [#include <libubox.h>])
AC_LIB_HAVE_LINKFLAGS(ubus, [], [#include <libubus.h>])

# Sysvinit
AC_ARG_ENABLE([sysvinit],
    AS_HELP_STRING([--enable-sysvinit], [Enable sysvinit @<:@default=yes@:>@]),
    [AS_CASE(${enableval}, [yes], [], [no], [],
             [AC_MSG_ERROR([bad value ${enableval} for --enable-sysvinit])])],
    [enable_sysvinit=yes])
AM_CONDITIONAL([HAVE_SYSVINIT], [test ! "x$enable_sysvinit" = x])

if test "x$enable_sysvinit" = xyes ; then
    if test -d /etc/init.d ; then
        sysvdir=/etc/init.d
    elif test -d /etc/rc.d/init.d ; then
       	sysvdir=/etc/rc.d/init.d
	else
       	AC_MSG_ERROR([
        --------------------------
        missing sysvinit directory
        --------------------------])
	fi
fi
AC_SUBST(sysvdir)

# Runit
AC_ARG_ENABLE([runit],
    AS_HELP_STRING([--enable-runit], [Enable runit @<:@default=yes@:>@]),
    [AS_CASE(${enableval}, [yes], [], [no], [],
             [AC_MSG_ERROR([bad value ${enableval} for --enable-runit])])],
    [enable_runit=yes])
AM_CONDITIONAL([HAVE_RUNIT], [test "x$enable_runit" = xyes])

# Runit supervision directory
AC_ARG_WITH(supervisedir, AS_HELP_STRING(--with-supervisedir=DIR,Runit supervise directory))
# (yes/no) are invalid values
test "x$with_supervisedir" = xyes -o "x$with_supervisedir" = xno && with_supervisedir=
if test "x$with_supervisedir" = x ; then
   AC_SUBST([DEB_HOST_MULTIARCH],[$(echo $(dpkg-architecture -qDEB_HOST_MULTIARCH 2>/dev/null || true))])
   if test "x$DEB_HOST_MULTIARCH" = x ; then
      supervisedir="/run/runit/usbmount.supervise"
   else
      supervisedir="/run/runit/supervise/usbmount"
   fi
else 
   supervisedir="$with_supervisedir"
fi
AC_SUBST(supervisedir)

# acl.d location
AC_ARG_WITH(aclsdir, AS_HELP_STRING(--with-aclsdir=DIR,Install acls in DIR))
# (yes/no) are invalid values
test "x$with_aclsdir" = xyes -o "x$with_aclsdir" = xno && with_aclsdir=
if test "x$with_aclsdir" = x ; then
    if test -d ${prefix}/share/acl.d ; then
        aclsdir=${prefix}/share/acl.d
    elif test -d /usr/share/acl.d ; then
        aclsdir=/usr/share/acl.d
    else
       	AC_MSG_ERROR([
       	-----------------------
       	missing acl.d directory
       	-----------------------])
	fi
else 
   aclsdir="$with_aclsdir"
fi
AC_SUBST(aclsdir)

# sysconfdir paths: DO NOT install in /etc/local...
AC_SUBST([sysconfdir],[${rootprefix}/etc])
AC_SUBST([usbmountconfdir],[${sysconfdir}/usbmount])

# -----------------------------------------------------------------------------

AC_CONFIG_FILES([Makefile
	src/Makefile
	conf/Makefile])
#	po/Makefile.in)

AC_OUTPUT
# ------------------------------------------------------------------------------

AC_MSG_RESULT([
        prefix:                  ${prefix}
        exec_prefix:             ${exec_prefix}
        sbindir:                 ${sbindir}

        aclsdir:                 ${aclsdir}
        supervisedir             ${supervisedir}
])

# ------------------------------------------------------------------------------
