/*
 * devpath.c
 * 
 * browse /sys/devices to find a subdirectory with the name of a device
 * 
 * Copyright (C) 2019 Didier Kryn <kryn@in2p3.fr>
 * 
 * All modifications to the original source file are:
 *  - Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org> 
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the LISENCE file.
 */


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#include "def.h"
#include "devpath.h"

/* hidden function which does all the work: */
static char *digsys(const char *, const char *, char *namebuf);

/*---------------------- The caller's interface -------------------------*/
/* Warning: the size of namebuf is unknown to the function. Better provide
   NAME_MAX+1 bytes. NAME_MAX+1 is defined in limits.h and dirent.h      */
char *devpath(const char *devname, char *namebuf)
{
  return digsys("/sys/devices", devname, namebuf);
}

/* dig recursively into pathname to find a subdir named findname */
/* we use lstat because we don't want to dereference symlinks    */
static char *digsys( const char *pathname, const char *findname,
		     char namebuf[NAME_MAX+1] )
{
  struct stat mystat;
  char *foundname;
  DIR *dirp;
  struct dirent *bdir;
  struct stat   bstat;
  int pathsize;

  foundname = NULL;
  if( lstat(pathname, &mystat) ) return NULL;
  /* Only consider directories */
  if ( ! S_ISDIR(mystat.st_mode) ) return NULL;
  dirp = opendir ( pathname );
  if( !dirp )
    {
      log_error( "%s: cannot open %s: %s\n", progname, pathname, strerror(errno) );
      exit(EXIT_FAILURE);
    }
  while ( (bdir=readdir(dirp)) )
    {
      if (!strcmp(bdir->d_name, ".") || !strcmp(bdir->d_name, "..") )
	continue;
      pathsize = strlen(pathname) + strlen(bdir->d_name) + 2;
      {
	char newpath[pathsize];
	strcpy(newpath, pathname);
	strcat(newpath, "/");
	strcat(newpath, bdir->d_name);
	lstat (newpath, &bstat);
	/* only consider directories */
	if (S_ISDIR(bstat.st_mode) )
	  {
	    if( !strcmp(bdir->d_name, findname) )
	      {
		/* BINGO ! */
		strcpy(namebuf, pathname);
		strcat(namebuf, "/");
		strcat(namebuf, findname);
		foundname = namebuf;
		break;
	      }
	    else
	      {
		/* otherwise invoke ourself recursively */
		foundname = digsys ( newpath, findname, namebuf );
		if(foundname) break;
	      }
	  }
      }
    }
  closedir(dirp);
  return foundname;
}
