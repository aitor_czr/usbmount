/*
 * init_list.h
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 

#ifndef __INIT_LIST_H__
#define __INIT_LIST_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	
   ino_t inode;
   int removable;
   char major_minor[256];
   char devtype_str[256];
   char sysfs_path[8193];
   
} node_t;

struct nodeList {
	
   node_t nodeT;
   struct nodeList *next;
};

int get_major_minor(char const*, char*);

struct nodeList *get_devices(struct nodeList*);
struct nodeList *add_new_node(struct nodeList*, char const*, ino_t);
struct nodeList *insert_new_node(struct nodeList*, node_t, int (*f)(node_t, node_t));
struct nodeList *clearList(struct nodeList*);

#endif /* __INIT_LIST_H__ */
