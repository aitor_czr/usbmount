 /*
  * main.c
  * 
  * This program is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <libgen.h>     // dirname() and basename()
#include <ctype.h>      // isdigit()	  
#include <errno.h>

#include <ubus/libubus.h>
#include <libubox/blobmsg_json.h>

#include "def.h"
#include "init_list.h"

#define MAX_SIZE 256

const char *progname="usbmount";

static const char *pid_file="/var/run/usbmount/usbmount.pid";
static bool foreground = false;
static int pid_fd = -1;  // lock file

static struct ubus_context *ctx;
static struct ubus_request_data req_data;
static struct blob_buf bb_t;

void parse_args(int argc, char *argv[]);
pid_t get_pid_t(void);
int wait_on_kill(int s, int m);
void daemonize(void);

enum {
	MOUNT_ID,
	MOUNT_UID,
	MOUNT_GID,
	MOUNT_DEVICE,
	MOUNT_MTPT,
	__MOUNT_MAX,
};
 
static const struct blobmsg_policy mount_policy[] =
{
	[MOUNT_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32},
	[MOUNT_UID]  = { .name="uid", .type=BLOBMSG_TYPE_INT32},
	[MOUNT_GID]  = { .name="gid", .type=BLOBMSG_TYPE_INT32},
	[MOUNT_DEVICE] = { .name="device", .type=BLOBMSG_TYPE_STRING },
	[MOUNT_MTPT] = { .name="mountpoint", .type=BLOBMSG_TYPE_STRING },
};
 
enum {
	UMOUNT_ID,
	UMOUNT_DEVICE,
	__UMOUNT_MAX,
};
 
static const struct blobmsg_policy umount_policy[] =
{
	[UMOUNT_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32},
	[UMOUNT_DEVICE] = { .name="device", .type=BLOBMSG_TYPE_STRING },
};
 
enum {
	REMOVE_ID,
	REMOVE_DEVICE,
	__REMOVE_MAX,
};
 
static const struct blobmsg_policy remove_policy[] =
{
	[REMOVE_ID]  = { .name="id", .type=BLOBMSG_TYPE_INT32},
	[REMOVE_DEVICE] = { .name="device", .type=BLOBMSG_TYPE_STRING },
};

static int mount_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
	char cmd[128] = {0};
	int rc = 0;
	char target[1024]={0};
    struct nodeList *l = NULL;
    bool removable = true;
    char *bname = NULL;
    char *uid_str = NULL;
    char *gid_str = NULL;    

	struct blob_attr *tb[__MOUNT_MAX];

	blobmsg_parse(mount_policy, ARRAY_SIZE(mount_policy), tb, blob_data(msg_t), blob_len(msg_t));
	
	const char *dname = dirname(strdup(blobmsg_data(tb[MOUNT_DEVICE])));
	bname = rindex(blobmsg_data(tb[MOUNT_DEVICE]), '/');
	
	if(strcmp(dname, "/dev")) return -1;
/*   
    l = get_devices(l);
   
    struct nodeList *aux = l;
    while(aux) {
       char* tmp = rindex( aux->nodeT.sysfs_path, '/' );
	   if(!strcmp(bname, tmp)) {
		   if(!aux->nodeT.removable) {
	          removable = false;
           }
           break;
       }
       
       aux = aux->next;
    }
            
    if(l) 
       l = clearList(l);
       
    if(removable) return 0;
    * */
	
    rc = mkdir(blobmsg_data(tb[MOUNT_MTPT]), 0755);
    if (rc == -1) {
        switch (errno) {
            case EACCES :
                log_error("%s: the parent directory does not allow write\n", progname);
                exit(EXIT_FAILURE);
            case EEXIST:
                log_info("%s: pathname already exists\n", progname);
                break;
            case ENAMETOOLONG:
                log_error("%s: pathname is too long\n", progname);
                exit(EXIT_FAILURE);
            default:
                perror("mkdir");
                exit(EXIT_FAILURE);
        }
    }
    	
    sprintf(target, blobmsg_data(tb[MOUNT_MTPT]));
    strcat(target, bname);
    	
    rc = mkdir(target, 0755);
    if (rc == -1) {
        switch (errno) {
            case EACCES :
                log_error("%s: parent directory does not allow write\n", progname);
                exit(EXIT_FAILURE);
            case EEXIST:
                log_info("%s: pathname already exists\n", progname);
                break;
            case ENAMETOOLONG:
                log_error("%s: pathname is too long\n", progname);
                exit(EXIT_FAILURE);
            default:
                perror("mkdir");
                exit(EXIT_FAILURE);
        }
    }
    
    if(asprintf(&uid_str, "%d", blobmsg_get_u32(tb[MOUNT_UID])) == -1) {
       printf("Unable to get uid: %s\n", strerror(errno));
       exit(EXIT_FAILURE);
    }

    if(asprintf(&gid_str, "%d", blobmsg_get_u32(tb[MOUNT_GID])) == -1) {
       printf("Unable to get gid: %s\n", strerror(errno));
       exit(EXIT_FAILURE);
    }
    
    sprintf(cmd, "chown %s:%s %s", uid_str, gid_str, target);
    system(cmd);
    
	memset(&cmd, 0, 128);
    sprintf(cmd, "mkdir -p /tmp/aitor/%s_%s", uid_str, gid_str);
    system(cmd);
	
	memset(&cmd, 0, 128);
    sprintf(cmd, "mount -o uid=%s,gid=%s /dev%s %s", uid_str, gid_str, bname, target);
    rc = system(cmd);
	
	// The above command may fail because it's not possible to force an owner 
	// on a disk with an ext4 filesystem (for the sake of argument), getting
	// WEXITSTATUS(rc) = 32. Only filesystems which do not support Linux 
	// permissions like fat have an attribute for ownership/groupship: 
	// uid=value and gid=value. See the man page of mount.
	if(WEXITSTATUS(rc) != 0) {
		memset(&cmd, 0, 128);	
        sprintf(cmd, "mount --rw /dev");
        strcat(cmd, bname);
        strcat(cmd, " ");
        strcat(cmd, target);
        system(cmd);
    }	

	blob_buf_init(&bb_t, 0);
	blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", cmd);
	ubus_send_reply(ctx, req, bb_t.head);

	ubus_defer_request(ctx, req, &req_data);
	ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);
	
	return 0;
}

static int umount_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
	char cmd[64] = {0};
	char device[64]={0};
    struct nodeList *l = NULL;
    bool found = false;
    char line[300];
    FILE *pf = NULL;
    int len = 0;
    char *bname = NULL;

	struct blob_attr *tb[__UMOUNT_MAX]; 

	blobmsg_parse(umount_policy, ARRAY_SIZE(umount_policy), tb, blob_data(msg_t), blob_len(msg_t));
	
	const char *dname = dirname(strdup(blobmsg_data(tb[UMOUNT_DEVICE])));
	bname = rindex(blobmsg_data(tb[UMOUNT_DEVICE]), '/');
	
	if(strcmp(dname, "/dev")) return -1;
   
    l = get_devices(l);
   
    struct nodeList *aux = l;
    while(aux) {
       char* tmp = rindex( aux->nodeT.sysfs_path, '/' );
	   if(!strcmp(bname, tmp)) {
		   sprintf(device, bname + 1);
		   found = true;
		   break;
       }
       
       aux = aux->next;
    }
        
    if(l) 
       l = clearList(l);
       
    if(!found) return -1;
       
    pf=fopen("/proc/self/mountinfo", "r");
    if(!pf) {
	   log_error("%s: Pipe error\n", progname);
	   exit(EXIT_FAILURE);
	}   

    while( fgets(line, sizeof(line), pf) ) {
       int mp[2] = {0,0}, fs[2], df[2] = {0,0};
	   char *mtpt, *devf, *partitionname;

	   sscanf(line, "%*s %*s %*s %*s %n%*s%n%*[^-]- %n%*s%n %n%*s%n",
	          mp, mp+1, fs, fs+1, df, df+1);
	   mtpt = line+mp[0];
	   devf = line+df[0];
	   printf("->%s\n", mtpt);
	   line[mp[1]] = line[fs[1]] = line[df[1]] = '\0';
	   partitionname = basename(devf);
	   len=strlen(partitionname);
	   if(!strcmp(partitionname, device)) {
		  char hidden[256] = {0};
          sprintf(cmd, "umount /dev%s", bname);
          system(cmd);
          sprintf(hidden, mtpt);
          strcat(hidden, "/.created_by_pmount");
          if( access( hidden, F_OK ) == 0 ) {
             unlink(hidden);
          }         
	      rmdir(mtpt);
	      break;
	   }
    }
    
    fclose(pf);
	blob_buf_init(&bb_t, 0);
	blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", cmd);
	ubus_send_reply(ctx, req, bb_t.head);

	ubus_defer_request(ctx, req, &req_data);
	ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);
	
	return 0;
}

static int remove_handler( struct ubus_context *ctx, struct ubus_object *obj,
			  struct ubus_request_data *req, const char *method,
			  struct blob_attr *msg_t )
{
	char cmd[64] = {0};
	char device[64]={0};
    struct nodeList *l = NULL;
    char line[300];
    FILE *pf = NULL;
    bool unmounted = true;
    int len = 0;
    char *bname = NULL;

	struct blob_attr *tb[__REMOVE_MAX]; 

	blobmsg_parse(remove_policy, ARRAY_SIZE(remove_policy), tb, blob_data(msg_t), blob_len(msg_t));
	
	const char *dname = dirname(strdup(blobmsg_data(tb[REMOVE_DEVICE])));
	bname = rindex(blobmsg_data(tb[REMOVE_DEVICE]), '/');
	
	if(strcmp(dname, "/dev")) return -1;
   
    l = get_devices(l);
   
    struct nodeList *aux = l;
    while(aux) {
       char* tmp = rindex( aux->nodeT.sysfs_path, '/' );
	   if(!strcmp(bname, tmp)) {
		   sprintf(device, bname + 1);
		   if(aux->nodeT.removable) {
	          sprintf(cmd, "/usr/bin/eject /dev/");
			  if(!strcmp(aux->nodeT.devtype_str, "partition")) {
			     int len = strlen(device);
			     device[len-1]='\0';
                 strcat(cmd, device);
              } else if(!strcmp(aux->nodeT.devtype_str, "disk")) {
                 strcat(cmd, device);
              }
	       } else {
              sprintf(cmd, "echo 1 | tee /sys/block/");
			  if(!strcmp(aux->nodeT.devtype_str, "partition")) {
			     int len = strlen(device);
			     device[len-1]='\0';
                 strcat(cmd, device);
                 strcat(cmd, "/device/delete");
              } else if(!strcmp(aux->nodeT.devtype_str, "disk")) {
                 strcat(cmd, device);
                 strcat(cmd, "/device/delete");
              }
           }
           break;
       }
       
       aux = aux->next;
    }
            
    if(l) 
       l = clearList(l);
       
    pf=fopen("/proc/self/mountinfo", "r");
    if(!pf) {
	   log_error("%s: Pipe error\n", progname);
	   exit(EXIT_FAILURE);
	}   

    while( fgets(line, sizeof(line), pf) ) {
	   int mp[2] = {0,0}, fs[2], df[2] = {0,0};
	   char *devf, *partitionname;

	   sscanf(line, "%*s %*s %*s %*s %n%*s%n%*[^-]- %n%*s%n %n%*s%n",
	          mp, mp+1, fs, fs+1, df, df+1);
	   devf = line+df[0];
	   line[mp[1]] = line[fs[1]] = line[df[1]] = '\0';
	   partitionname = basename(devf);
	   len=strlen(partitionname);
	   if(isdigit(partitionname[len-1])) partitionname[len-1] = '\0';
	   if(!strcmp(partitionname, device)) {
	      unmounted = false;
	      break;
	   }
    }
    
    fclose(pf);
       
    if(!unmounted) return -1;    

	system(cmd); 

	blob_buf_init(&bb_t, 0);
	blobmsg_add_string(&bb_t,"Server reply - Request has been proceeded: ", cmd);
	ubus_send_reply(ctx, req, bb_t.head);

	ubus_defer_request(ctx, req, &req_data);
	ubus_complete_deferred_request(ctx, req, UBUS_STATUS_OK);
	
	return 0;
}
 

static const struct ubus_method usbmount_methods[] =
{
	UBUS_METHOD("mount", mount_handler, mount_policy),
	UBUS_METHOD("umount", umount_handler, umount_policy),
	UBUS_METHOD("remove", remove_handler, remove_policy),
};
 

static struct ubus_object_type usbmount_obj_type =
	UBUS_OBJECT_TYPE("ering.usbmount", usbmount_methods);
 

static struct ubus_object usbmount_obj=
{
	.name = "ering.usbmount", 
	.type = &usbmount_obj_type,
	.methods = usbmount_methods,
	.n_methods = ARRAY_SIZE(usbmount_methods),
	//.path=
};


int main(int argc, char **argv)
{      
   int ret;
   const char *ubus_socket="/var/run/ubus/ubus.sock";
   struct sigaction sa;
	
   parse_args(argc, argv);
   
	if (foreground == 0) {
		
		daemonize();
	}

	openlog(argv[0], LOG_PID|LOG_CONS, LOG_DAEMON);
	syslog(LOG_INFO, "Started %s", progname);
	
	uloop_init();
 
	ctx=ubus_connect(ubus_socket);
	if(ctx==NULL) exit(EXIT_FAILURE);
 
	ubus_add_uloop(ctx);

	ret=ubus_add_object(ctx, &usbmount_obj);
	if(ret!=0) goto UBUS_FAIL;
	
	uloop_run();
	uloop_done();
	
UBUS_FAIL:
   ubus_free(ctx);

   if( access( pid_file, F_OK ) == 0 ) {
      if(pid_fd != -1) {
         int rc = lockf(pid_fd, F_ULOCK, 0);
         if(rc != 0) exit(EXIT_FAILURE);
         close(pid_fd);
      }        
      unlink(pid_file);
   }
   
   syslog(LOG_INFO, "Stopped %s", progname);
   closelog();
   
   return 0;
}

void parse_args(int argc, char *argv[])
{
   static struct option long_options[] = {
	   
      {"foreground",            no_argument, 0, 'f'},
      {"kill",                  no_argument, 0, 'k'},
      {"check-running",         no_argument, 0, 'c'},
      {0, 0, 0}
   };
   
   int value, option_index = 0;
   int _kill = 0, _check = 0;
   while ((value = getopt_long(argc, argv, "fkc", long_options, &option_index)) != -1) {
      switch (value) {
         case 0:
            if (long_options[option_index].flag)
            break;
            
         case 'f':
            foreground = true;
            break;
                
         case 'k':
            _kill = 1;
            break;
            
         case 'c':
            _check = 1;
            break;
                  
         default:
            syslog(LOG_ERR, "Unknown parameter.");
            break;
      }
      
   }
  
   if (_kill) {

      int rc = 0;
      
      if ((rc = wait_on_kill(SIGTERM, 60)) < 0) {
         log_error("Failed to kill daemon with SIGTERM: %s\n", strerror(errno));
         exit(1);
      }
        
      exit(EXIT_SUCCESS);
   }
   
   if (_check) {
      
      pid_t pid = get_pid_t();
      
       if (pid == (pid_t) -1 || pid == 0) {
          // Do not print anything here. Empty output is used in snetaid.postinst script 
          // to check whether or not sysvinit is supervising snetaid
          // printf("%s not running.\n", progname);
          exit(EXIT_FAILURE);
       } else {
          printf("%s process running as pid %u.\n", progname, pid);
          exit(EXIT_SUCCESS);
       }
   }
}
   
/**
 * \brief Create the directory to store the pidfile and the logfile.
 */
static
void mkdir_piddir(const char *s)
{
	char *piddir, *tmp;

	piddir = strdup(s);
	tmp = strrchr(piddir, '/');
	if (tmp) {
		*tmp = '\0';
		mkdir(piddir, 0755);
	}
	free(piddir);
}
   
/**
 * \brief Get the PID of the process.
 */
pid_t get_pid_t(void)
{
   static char txt[MAX_SIZE];
   int fd = -1;
   pid_t ret = (pid_t) -1, pid;
   ssize_t l;
   long lpid;
   char *e = NULL;
   
   if ((fd = open(pid_file, O_RDWR, 0644)) < 0) {
      if ((fd = open(pid_file, O_RDONLY, 0644)) < 0) {
         if (errno != ENOENT) {
         log_error("Failed to open pidfile '%s': %s , errno=%d\n", pid_file, strerror(errno), errno);
         }
         goto finish;
      }
   }
   
   if ((l = read(fd, txt, MAX_SIZE-1)) < 0) {
      int saved_errno = errno;
      log_error("read(): %s , errno=%d", strerror(errno), errno);
      unlink(pid_file);
      errno = saved_errno;
      goto finish;
   }
   
   txt[l] = 0;
   txt[strcspn(txt, "\r\n")] = 0;
   
   errno = 0;
   lpid = strtol(txt, &e, 10);
   pid = (pid_t) lpid;
   
   if (errno != 0 || !e || *e || (long) pid != lpid) {
      unlink(pid_file);
      errno = EINVAL;
      log_warn("PID file corrupt, removing. (%s) , errno=%d", pid_file, errno);
      goto finish;
   }
   
   if (kill(pid, 0) != 0 && errno != EPERM) {
      int saved_errno = errno;
      log_warn("Process %lu died: %s; trying to remove PID file. (%s)", (unsigned long) pid, strerror(errno), pid_file);
      unlink(pid_file);
      errno = saved_errno;
      goto finish;
   }
   
   ret = pid;

finish:
   if (fd != -1) {
      int rc = 0;
      if((rc=lockf(fd, F_ULOCK, 0)) < 0) {
		  log_error("%s: Cannot unlock file\n", progname);
		  exit(EXIT_FAILURE);
	  }
	  close(fd);
   }
   
   return ret;
}

/**
 * \brief Kill the daemon process.
 */
int wait_on_kill(int s, int m)
{
   pid_t pid;
   time_t t;

   if ((pid = get_pid_t()) < 0) 
      return -1;

   if (kill(pid, s) < 0)
      return -1;

   t = time(NULL) + m;

   for (;;) {
      int r;
      struct timeval tv = { 0, 100000 };

      if (time(NULL) > t) {
         errno = ETIME;
         return -1;
      }

      if ((r = kill(pid, 0)) < 0 && errno != ESRCH)
         return -1;

      if (r)
         return 0;

      if (select(0, NULL, NULL, NULL, &tv) < 0)
         return -1;
    }
}

/**
 * \brief This function will daemonize this app
 */
void daemonize()
{
   int ret;
   pid_t pid = 0;
   int fd;

   /* Fork off the parent process.
    * The first fork will change our pid
    * but the sid and pgid will be the
    * calling process
    */
   pid = fork();

   /* An error occurred */
   if (pid < 0) {
      exit(EXIT_FAILURE);
   }

   /* Fork off for the second time.
    * The magical double fork. We're the session
    * leader from the code above. Since only the
    * session leader can take control of a tty
    * we will fork and exit the session leader.
    * Once the fork is done below and we use
    * the child process we will ensure we're
    * not the session leader, thus, we cannot
    * take control of a tty.
    */
   if (pid > 0) {
      exit(EXIT_SUCCESS);
   }

   /* On success: The child process becomes session leader */
   if (setsid() < 0) {
      exit(EXIT_FAILURE);
   }

   /* Ignore signal sent from child to parent process */
   signal(SIGCHLD, SIG_IGN);

   /* Fork off for the second time*/
   pid = fork();

   /* An error occurred */
   if (pid < 0) {
      exit(EXIT_FAILURE);
   }

   /* Success: Let the parent terminate */
   if (pid > 0) {
      exit(EXIT_SUCCESS);
   }
   
   /* Set new file permissions */
   umask(0);

   /* Change the working directory to /tmp */
   /* or another appropriated directory */
   ret = chdir("/tmp");
   if(ret != 0) exit(EXIT_FAILURE);

   /* Close all open file descriptors */
   for (fd = sysconf(_SC_OPEN_MAX); fd > 0; fd--) {
      close(fd);
   }

   /* Reopen stdin (fd = 0), stdout (fd = 1), stderr (fd = 2) */
   stdin  = fopen("/dev/null", "r");
   stdout = fopen("/dev/null", "w+");
   stderr = fopen("/dev/null", "w+");
   
   /* Try to write PID of daemon to lockfile */
   if (pid_file)
   {
	  mkdir_piddir(pid_file);
	  
      pid_fd = open(pid_file, O_RDWR|O_CREAT, 0640);
      if (pid_fd < 0) { 
		 log_error("%s: Cannot open pidfile: %s\n", progname, strerror(errno));
         exit(EXIT_FAILURE);
      }
      
      if (lockf(pid_fd, F_TLOCK, 0) < 0) {
         /* Can't lock file */
         exit(EXIT_FAILURE);
      }

      // Get current PID
      char *mypid = NULL;
      if(asprintf(&mypid, "%jd", (intmax_t) getpid()) != -1) {
         ssize_t sz = 0;
         // Write PID to lockfile
         sz = write(pid_fd, mypid, strlen(mypid));
         if(sz == -1) { 
		    log_error("%s: Unable to write pid: %s\n", progname, strerror(errno));
			exit(EXIT_FAILURE);
	     }
	  } else {  
		 log_error("%s: Unable to write pid: %s\n", progname, strerror(errno));
		 exit(EXIT_FAILURE);
	  }
   }
}
