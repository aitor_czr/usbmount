/*
 * init_list.c
 * 
 * Read the current directory (assuming it is the root of the device files
 * tree (normally /dev) and invoke pm_add_part() for each entry of the
 * directory. pm_add_part() will select the device files which make sense.
 * 
 * Copyright (C) 2019 Didier Kryn <kryn@in2p3.fr>
 * 
 * All modifications to the original source file are:
 *  - Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org> 
 * 
 * Hopman is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Hopman is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the LISENCE file.
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <libgen.h>     // basename()
#include <ctype.h>
#include <fcntl.h>
#include <stdbool.h>
#include <math.h>
#include <errno.h>

#include "def.h"
#include "devpath.h"
#include "init_list.h"
#include "sysfs.h"

const char *mountpoint="/dev/";

struct nodeList *get_devices(struct nodeList *list)
{  
  DIR *dirp;
  struct dirent *E;

  /*------------------------------ Scan /dev --------------------------------*/
  
   // Make the mountpoint directory current
   if(chdir(mountpoint))
     {
        log_error("%s: Cannot make %s current directory\n", progname, mountpoint);
        exit(EXIT_FAILURE);
     }
     
  dirp = opendir(".");
  if(!dirp)
    {
      fprintf(stderr, "%s error opening current directory: %s\n",
	      progname, strerror(errno));
      exit(EXIT_FAILURE);
    }

  /* Invoke partition_new() for all devices associated to hotplug partitions */
  while( (errno=0, E=readdir(dirp)) )
    {
      struct stat devstat;
      if( lstat(E->d_name, &devstat) )
	{
	  log_error("%s: create unknown device %s\n", progname, E->d_name);
	  exit(EXIT_FAILURE);
	}
      if( (devstat.st_mode & S_IFMT) == S_IFBLK )
	{
	  char namebuf[NAME_MAX+1], *pathname;
      pathname = (char*)malloc(sizeof(char) * NAME_MAX+1);
      if (!pathname) {
        log_error("%s: Memory allocation failure errno = %s\n", progname, strerror(errno));
        exit(EXIT_FAILURE);
      }
                    
      memset(pathname, 0, NAME_MAX);
      if(strlen(pathname)==0) {
        if( !(pathname=devpath(E->d_name, namebuf)) ) {
          fprintf( stderr, "%s Cannot find sys path for %s. errno = %s\n", 
              progname, E->d_name, strerror(errno) );
          continue;
        }
      }
                     
      list = add_new_node(list, pathname, E->d_ino);
	}
    }
  if(errno)
    {
      log_error("%s: error reading current directory: %s\n", progname, strerror(errno));
      exit(EXIT_FAILURE);
    }
   closedir(dirp);
   
   return list;
}


// find out mayor and minor numbers
int get_major_minor( char const* sysfs_path, char* device_major_minor )
{ 
   char uevent_path[4097];
   memset( uevent_path, 0, 4097 );
   
   char *uevent_buf=NULL;
   size_t uevent_len = 0;
   
   int rc = 0;
    
   snprintf(uevent_path, 4096, "%s/dev", sysfs_path );
   
   rc = io_read_file( uevent_path, &uevent_buf, &uevent_len );
   if( rc != 0 ) {
      
      return rc;
   }
   
   uevent_buf[uevent_len-1]='\0';
   strcpy( device_major_minor, uevent_buf);
   
   //free( uevent_buf.buf );
   
   return 0;
}

static inline int compare_nodes( node_t a, node_t b )
{
   char *r = strdup(a.major_minor);
   char *s = strdup(b.major_minor);
   r[0] = r[1] = '0';
   s[0] = s[1] = '0'; 
   return ( atoi(r) - atoi(s) );
}

// find out what kind of device this is from the sysfs device tree 
// fill in the caller-supplied pointer with a malloc'ed string that encodes the DEVTYPE 
int get_device_type( char const* sysfs_path, char** device_type, size_t* device_type_len ) {
   
   char uevent_path[4097];
   memset( uevent_path, 0, 4097 );
   
   char* uevent_buf = NULL;
   size_t uevent_len = 0;
   
   char* device_type_buf = NULL;
   size_t device_type_buf_len = 0;
   
   int rc = 0;
   
   snprintf(uevent_path, 4096, "%s/uevent", sysfs_path );
   
   rc = io_read_file( uevent_path, &uevent_buf, &uevent_len );
   if( rc != 0 ) {
      
      return rc;
   }
   
   rc = sysfs_uevent_get_key( uevent_buf, uevent_len, "DEVTYPE", &device_type_buf, &device_type_buf_len );
   if( rc != 0 ) {
      
      free( uevent_buf );
      return rc;
   }
   
   *device_type = device_type_buf;
   *device_type_len = device_type_buf_len;
   
   free( uevent_buf );
   
   return 0;
}

struct nodeList *add_new_node( struct nodeList *list, char const *sysfs_base, ino_t d_ino )
{
   node_t item;
   int (*f)( node_t, node_t );  
   int rc;
   
   // interface information     
   char device_major_minor[256] = {0};
   
   // interface information   
   char* devtype_str = NULL;
   size_t devtype_strlen = 0;
   
   char* if_device_path = NULL;
   size_t if_device_path_len= 0;
   
   item.inode = d_ino;
   
   if( strlen(sysfs_base) >= 4096 ) {
      log_error("%s: Invalid /sys/devices path: %s \n", progname, sysfs_base);
      exit(3);
   }
   
   f=compare_nodes;
   strcpy( item.sysfs_path, sysfs_base );
   
   // find out what kind of device we are...
   rc = get_device_type( item.sysfs_path, &devtype_str, &devtype_strlen );
   if( rc == 0 ) {
		strcpy( item.devtype_str, devtype_str );
	}	
   
   // mayor and minor numbers
   rc = get_major_minor( item.sysfs_path, device_major_minor );
   if( rc == 0 ) {
		strcpy( item.major_minor, device_major_minor );
	}
   
   // find USB parent 
   rc = sysfs_get_parent_with_subsystem_devtype( sysfs_base, "usb", "usb_interface", &if_device_path, &if_device_path_len );
   if( rc == 0 ) {
	  char buffer[8193]={0};
   
   // Is the device removable?
   int removable = 0;
   char *temp = strdup(item.sysfs_path);
   if(!strcmp(devtype_str, "partition")) {
      int count = 0;
      while(temp[strlen(temp)-count-1] != '/') count++;
      temp[strlen(temp)-count-1] = '\0';
   }
    
   int fd, devfd; /* file descriptors */
   strcpy(buffer, temp);
   strcat(buffer, "/removable");
  
   /* open directory pathname is pointing to */ 
   if( (devfd=open(temp, O_RDONLY)) < 0 )
    {
      fprintf(stderr, "Cannot open %s %s\n", temp, strerror(errno));
      exit(EXIT_FAILURE);
    }

   /* open file "removable" in parent directory */
   if( (fd=openat(devfd, buffer, O_RDONLY)) < 0 )
    {
      fprintf(stderr,
	      "Cannot find in /sys if device of %s is removable.\n", 
	      temp);
      exit(EXIT_FAILURE);
    }

   /* read 1st character of this file */
   char c;
   if(read(fd, &c, 1) != 1)
    {
      fprintf(stderr,
	      "Cannot read if parent device of %s is removable: %s.\n", 
	      temp, strerror(errno));
      exit(EXIT_FAILURE);
    }

   removable = (c =='1');
   item.removable = removable; 
	   
	   
      list = insert_new_node(list, item, f);
  }
   
   return list;
}

/*
 Insert new node in the sorted list
 The new list remains ordered
 f is the function used to sort
*/
struct nodeList *insert_new_node( struct nodeList *list, node_t new_node, int (*f)( node_t a, node_t b ) )
{
	struct nodeList *p,*q,*prev;
	/* new node */
	q = (struct nodeList *) malloc(sizeof(struct nodeList));
	q->nodeT = new_node;
	q->next = NULL; /* by default, it can change */
	if (!list) {
		list = q;
		return list;
	}
	/* the  list is not empty */
	prev = NULL;
	p = list;
	while (p) {
		if ((*f)(new_node,p->nodeT) > 0) {
			prev = p;
			p = p->next;
		}
		else break;
	}
	if (!prev) { /* prepend */
		q->next = list;
		list = q;
	} else { /* insert in the middle or at the end */
		prev->next = q;
		q->next = p;
	}
	return list;
}
  
struct nodeList *clearList( struct nodeList *list )
{
	struct nodeList *p,*q;
	p = list;
	while(!p) {
		q = p->next; /* keep the node */
		free(p);
		p = q;
	}
	return NULL;
}


