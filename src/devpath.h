/*
 * devpath.h
 * 
 * browse /sys/devices to find a subdirectory with the name of a device
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the LISENCE file.
 */


#ifndef __DEVPATH_H__
#define __DEVPATH_H__

#include <stdio.h>
#include <stdlib.h>

/*---------------------- The caller's interface -------------------------*/
/* Warning: the size of namebuf is unknown to the function. Better provide
   NAME_MAX+1 bytes. NAME_MAX+1 is defined in limits.h and dirent.h      */
char *devpath(const char *devname, char *namebuf);

#endif // __DEVPATH_H__

