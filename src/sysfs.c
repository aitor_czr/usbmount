/*
 * sysfs.c
 * 
 * Forked from vdev: a virtual device manager for *nix
 * Copyright (C) 2014  Jude Nelson
 * (https://github.com/jcnelson/vdev)
 * 
 * All modifications to the original source are:
 * Copyright (C) 2021 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 *
 * Original copyright and license text produced below.
 */

/*
   This file is dual-licensed: you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3 or later as 
   published by the Free Software Foundation. For the terms of this 
   license, see LICENSE.GPLv3+ or <http://www.gnu.org/licenses/>.

   You are free to use this program under the terms of the GNU General
   Public License, but WITHOUT ANY WARRANTY; without even the implied 
   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.

   Alternatively, you are free to use this program under the terms of the 
   Internet Software Consortium License, but WITHOUT ANY WARRANTY; without
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   For the terms of this license, see LICENSE.ISC or 
   <http://www.isc.org/downloads/software-support-policy/isc-license/>.
*/


#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "sysfs.h"
#include "def.h"


// find a field value from a uevent 
// return 0 on success
// return -ENOENT if the key is not found 
// return -ENOMEM if oom.
int sysfs_uevent_get_key( char const* uevent_buf, size_t uevent_buflen, char const* key, char** value, size_t* value_len ) {
   
   char* tmp = NULL;
   char* tok = NULL;
   char* buf_ptr = NULL;
   char* delim = NULL;
   int rc = 0;
   
   // NOTE: zero-terminate
   char* buf_dup = (char*)calloc( uevent_buflen + 1, 1 );
   if( buf_dup == NULL ) {
      return -ENOMEM;
   }
   
   memcpy( buf_dup, uevent_buf, uevent_buflen );
   buf_ptr = buf_dup;
   
   while( 1 ) {
      
      // next line 
      tok = strtok_r( buf_ptr, "\n", &tmp );
      buf_ptr = NULL;
      
      if( tok == NULL ) {
         rc = -ENOENT;
         break;
      }
      
      // read key 
      delim = index( tok, '=' );
      if( delim == NULL ) {
         continue;
      }
      
      *delim = '\0';
      
      // match key?
      if( strcmp( tok, key ) == 0 ) {
         
         // get value
         *value_len = strlen( delim + 1 );
         *value = (char*)calloc( *value_len + 1, 1 );
         
         if( *value == NULL ) {
            rc = -ENOMEM;
            break;
         }
         
         strcpy( *value, delim + 1 );
         break;
      }
      
      *delim = '=';
   }
   
   free( buf_dup );
   return rc;
}


// walk up a sysfs device path to find the ancestor device with the given subsystem and devtype
// if devtype_name is NULL, match any devtype (only match the subsystem)
// return 0 on success
// return -ENOMEM on OOM 
// return -ENOENT if a subsystem or uevent was not found, when one was expected
int sysfs_get_parent_with_subsystem_devtype( char const* sysfs_device_path, char const* subsystem_name, char const* devtype_name, char** devpath, size_t* devpath_len ) {
   
   
   char cur_dir[4097];
   char subsystem_path[4097];
   char subsystem_link[4097];
   char uevent_path[4097];
   
   memset( subsystem_path, 0, 4097 );
   memset( subsystem_link, 0, 4097 );
   
   char* tmp = NULL;
   int rc = 0;
   char* uevent_buf = NULL;
   size_t uevent_len = 0;
   char* devtype = NULL;
   size_t devtype_len = 0;
   char* parent_device = NULL;
   size_t parent_device_len = 0;
   
   strcpy( cur_dir, sysfs_device_path );
   
   while( 1 ) {
      
      // get parent device 
      rc = sysfs_get_parent_device( cur_dir, &parent_device, &parent_device_len );
      if( rc != 0 ) {
         break;
      }
      
      // subsystem?
      sprintf( subsystem_path, "%s/subsystem", parent_device );
      
      memset( subsystem_link, 0, 4096 );
      
      rc = readlink( subsystem_path, subsystem_link, 4096 );
      if( rc < 0 ) {
         
         rc = -errno;
         if( rc != -ENOENT ) {
            fprintf(stderr, "[WARN]: readlink('%s') errno = %d\n", subsystem_path, rc );
         }
         
         free( parent_device );
         parent_device = NULL;
         return rc;
      }
      
      // get subsystem name...
      tmp = rindex( subsystem_link, '/' );
      
      if( tmp != NULL && strcmp( tmp + 1, subsystem_name ) != 0 ) {
         
         // subsystem does not match
         // crawl up to the parent
         strcpy( cur_dir, parent_device );
         
         free( parent_device );
         parent_device = NULL;
         continue;
      }
      
      // subsystem matches...
      *tmp = 0;
      
      if( devtype_name != NULL ) {
         
         // get uevent
         // get DEVTYPE from uevent 
         // make uevent path 
         sprintf( uevent_path, "%s/uevent", parent_device );
         
         // get uevent 
         rc = io_read_file( uevent_path, &uevent_buf, &uevent_len );
         if( rc < 0 ) {
            
            fprintf(stderr, "[WARN]: sysfs_read_file('%s') rc = %d\n", uevent_path, rc );
            
            free( parent_device );
            parent_device = NULL;
            return rc;
         }
         
         // get devtype 
         rc = sysfs_uevent_get_key( uevent_buf, uevent_len, "DEVTYPE", &devtype, &devtype_len );
         free( uevent_buf );
         
         if( rc != 0 ) {
            
            if( rc == -ENOENT ) {
               
               // not found 
               // next parent 
               strcpy( cur_dir, parent_device );
               
               free( parent_device );
               parent_device = NULL;
               
               continue;
            }
            else {
               
               free( parent_device );
               parent_device = NULL;
               
               return rc;
            }
         }
         
         // matches?
         if( strcmp( devtype, devtype_name ) == 0 ) {
            
            free( devtype );
            
            // this is the path to the device 
            *devpath = parent_device;
            *devpath_len = strlen( parent_device );
            
            // found!
            rc = 0;
            break;
         }
         else {
            // no match.
            // next device 
            free( devtype );
            
            strcpy( cur_dir, parent_device );
            
            free( parent_device );
            parent_device = NULL;
            continue;
         }
      }
      else {
         
         // match only on subsystem 
         *devpath = parent_device;
         *devpath_len = strlen(parent_device);
         
         rc = 0;
         break;
      }
   }
   
   return rc;
}

// get the parent device of a given device, using sysfs.
// not all devices have parents; those that do will have a uevent file.
// walk up the path until we reach /
// return 0 on success
// return -EINVAL if the dev path has no '/'
// return -ENOENT if this dev has no parent
// return -errno if stat'ing the parent path fails 
// return -ENOMEM if OOM
int sysfs_get_parent_device( char const* dev_path, char** ret_parent_device, size_t* ret_parent_device_len ) {
   
   char* parent_path = NULL;
   size_t parent_path_len = 0;
   struct stat sb;
   int rc = 0;
   
   char* delim = NULL;
   
   parent_path = (char*)calloc( strlen(dev_path) + 1 + strlen("/uevent"), 1 );
   if( parent_path == NULL ) {
      return -ENOMEM;
   }
   
   strcpy( parent_path, dev_path );
   
   while( strlen(parent_path) > 0 ) {
      
      // lop off the child 
      delim = rindex( parent_path, '/' );
      if( delim == NULL ) {
         
         // invalid 
         free( parent_path );
         return -EINVAL;
      }
      
      if( delim == parent_path ) {
         
         // reached /
         free( parent_path );
         return -ENOENT;
      }
      
      while( *delim == '/' && delim != parent_path ) {
         *delim = '\0';
         delim--;
      }
      
      parent_path_len = strlen( parent_path );
      
      // verify that this is a device...
      strcat( parent_path, "/uevent" );
      
      rc = stat( parent_path, &sb );
      if( rc != 0 ) {
         
         // not a device
         // remove /uevent 
         parent_path[ parent_path_len ] = '\0';
         continue;
      }
      else {
         
         // device!
         break;
      }
   }
   
   // lop off /uevent 
   parent_path[ parent_path_len ] = '\0';
   
   *ret_parent_device = parent_path;
   *ret_parent_device_len = parent_path_len;
   
   return 0;
}


// get the sysname from the device path
int sysfs_get_sysname( char const* devpath, char** ret_sysname, size_t* ret_sysname_len ) {
   
   char const* delim = NULL;
   char* sysname = NULL;
   size_t len = 0;
   
   delim = rindex( devpath, '/' );
   if( delim == NULL ) {
      
      return -EINVAL;
   }
   
   sysname = strdup( delim + 1 );
   
   if( sysname == NULL ) {
      return -ENOMEM;
   }
   
    /* some devices have '!' in their name, change that to '/' */
    while( sysname[len] != '\0' ) {
       
       if( sysname[len] == '!' ) {
          sysname[len] = '/';
       }
       
       len++;
    }
    
    *ret_sysname = sysname;
    *ret_sysname_len = len;
    
   return 0;
}


// read a file, masking EINTR
// return the number of bytes read on success
// return -errno on failure
ssize_t io_read_uninterrupted( int fd, char* buf, size_t len ) {
   
   ssize_t num_read = 0;
   
   if( buf == NULL ) {
      return -EINVAL;
   }
   
   while( (unsigned)num_read < len ) {
      ssize_t nr = read( fd, buf + num_read, len - num_read );
      if( nr < 0 ) {
         
         int errsv = -errno;
         if( errsv == -EINTR ) {
            continue;
         }
         
         return errsv;
      }
      if( nr == 0 ) {
         break;
      }
      
      num_read += nr;
   }
   
   return num_read;
}

// read a whole file into RAM
// return 0 on success, and set *file_buf and *file_buf_len 
// return negative on error
int io_read_file( char const* path, char** file_buf, size_t* file_buf_len ) {
   
   int fd = 0;
   struct stat sb;
   char* buf = NULL;
   int rc = 0;
   
   // get size 
   rc = stat( path, &sb );
   if( rc != 0 ) {
      
      rc = -errno;
      return rc;
   }
   
   // read the uevent file itself 
   buf = (char*)calloc( sb.st_size, 1 );
   if( buf == NULL ) {
      
      return -ENOMEM;
   }
   
   fd = open( path, O_RDONLY );
   if( fd < 0 ) {
      
      rc = -errno;
      free( buf );
      return rc;
   }
   
   rc = io_read_uninterrupted( fd, buf, sb.st_size );
   if( rc < 0 ) {
      
      close( fd );
      free( buf );
      return rc;
   }
   
   close( fd );
   
   *file_buf = buf;
   *file_buf_len = rc;
   
   return 0;
}
