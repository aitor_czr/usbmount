#!/bin/sh -e

test -n "$srcdir" || srcdir=`dirname "$0"`
test -n "$srcdir" || srcdir=.

clean_files() {
  rm -f ABOUT-NLS compile config.h config.h.in~ config.sub configure~ install-sh ltmain.sh Makefile.in stamp-h1  \
     aclocal.m4 config.guess config.h.in config.log config.status configure depcomp libtool Makefile missing
  rm -rf src/.deps \
  rm -f src/Makefile src/Makefile.in \
  rm -f conf/Makefile conf/Makefile.in
  rm -rf autom4te.cache
  rm -f po/*.sed po/*.header po/*.gmo po/*.sin po/Rules-quot po/stamp-it po/Makefile po/Makefile.in po/Makefile.in.in
}

configure() {
  echo "Regenerating autotools files"
  autoreconf --force --install --verbose --warnings=all "$srcdir"
  echo "Setting up Intltool"
  #intltoolize --copy --force --automake || exit 1
  $srcdir/configure \
	--enable-manpages \
	--prefix=/usr/local \
	"$@"
}

case $1 in
  clean)
    clean_files
    ;;
  cleanall)
    make clean
    clean_files
	;;	
  *)
    configure "$@"
    ;;
esac
